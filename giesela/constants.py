MAIN_VERSION = "5.5.4"
SUB_VERSION = "refreshed-ll"
VERSION = MAIN_VERSION + "_" + SUB_VERSION

CONFIG_LOCATION = "data/config.yml"

maj_versions = {
    "5": "Refreshed",
    "4": "Webiesela",
    "3": "Giesenesis"
}
